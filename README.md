# Use following command to run project
``` ./gradlew bootRun```

# After running successfully application run following commands in console to see results from exposed REST service
`curl 'http://localhost:8080/simulation/income?premium=PREMIUM&economy=ECONOMY'`
where instead of PREMIUM and ECONOMY you will pass number of available rooms of given type.

For example if you would like to calculate expected income for 7 premiums rooms and 5 economy rooms invoke following command in command line:
curl 'http://localhost:8080/simulation/income?premium=7&economy=5'