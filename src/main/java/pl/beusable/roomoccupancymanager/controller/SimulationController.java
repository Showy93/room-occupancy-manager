package pl.beusable.roomoccupancymanager.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.beusable.roomoccupancymanager.controller.model.AmountOfAvailableRooms;
import pl.beusable.roomoccupancymanager.service.SimulationService;
import pl.beusable.roomoccupancymanager.service.model.Income;

@RestController
@RequestMapping("/simulation")
@RequiredArgsConstructor
public class SimulationController {

	private final SimulationService simulationService;

	@GetMapping("/income")
	public Income calculateExpectedIncome(@RequestParam("premium") int premium, @RequestParam("economy") int economy) {
		return simulationService.calculateExpectedIncome(new AmountOfAvailableRooms(premium, economy));
	}
}
