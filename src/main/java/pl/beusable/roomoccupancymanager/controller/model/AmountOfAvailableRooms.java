package pl.beusable.roomoccupancymanager.controller.model;

import lombok.Value;

public record AmountOfAvailableRooms(int premium, int economy) {
}
