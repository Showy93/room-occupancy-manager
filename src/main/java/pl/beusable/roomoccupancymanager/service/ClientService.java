package pl.beusable.roomoccupancymanager.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import pl.beusable.roomoccupancymanager.service.model.Client;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ClientService {

	private final ObjectMapper objectMapper;

	@SneakyThrows
	public List<Client> getPotentialClients() {
		File file = ResourceUtils.getFile("classpath:static/clients.json");
		TypeReference<List<BigDecimal>> listTypeReference = new TypeReference<>() {};
		// TODO ClientDeserializer
		List<BigDecimal> listOfAmountsToPay = objectMapper.readValue(file, listTypeReference);
		return IntStream.range(0, listOfAmountsToPay.size())
				.mapToObj(index -> new Client(index, listOfAmountsToPay.get(index)))
				.toList();
	}
}
