package pl.beusable.roomoccupancymanager.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.beusable.roomoccupancymanager.controller.model.AmountOfAvailableRooms;
import pl.beusable.roomoccupancymanager.service.model.Client;
import pl.beusable.roomoccupancymanager.service.model.Income;
import pl.beusable.roomoccupancymanager.service.model.Room;
import pl.beusable.roomoccupancymanager.service.model.RoomOccupancyAndIncome;
import pl.beusable.roomoccupancymanager.service.model.enums.RoomStandard;
import pl.beusable.roomoccupancymanager.service.roomallocation.RoomAllocatorStrategy;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class SimulationService {

	private final ClientService clientService;
	private final List<RoomAllocatorStrategy> roomAllocators;


	public Income calculateExpectedIncome(AmountOfAvailableRooms amountOfAvailableRooms) {
		List<Client> clients = clientService.getPotentialClients();
		List<Room> rooms = generateRooms(amountOfAvailableRooms);
		List<Client> clientsOrderedDescendingByAmountToPay = clients
				.stream()
				.sorted((o1, o2) -> o2.amountToPay().compareTo(o1.amountToPay()))
				.toList();
		clientsOrderedDescendingByAmountToPay.forEach(client -> roomAllocators.stream()
				.filter(roomAllocatorStrategy -> roomAllocatorStrategy.test(client))
				.findAny()
				.ifPresent(roomAllocatorStrategy -> roomAllocatorStrategy.allocateClientToAnyRoomIfPossible(client, rooms)));
		return mapRoomsToIncome(rooms);
	}

	private List<Room> generateRooms(AmountOfAvailableRooms amountOfAvailableRooms) {
		Stream<Room> economyRooms = IntStream.rangeClosed(1, amountOfAvailableRooms.economy())
				.mapToObj(index -> Room.builder().roomStandard(RoomStandard.ECONOMY).index(index).build());
		Stream<Room> premiumRooms = IntStream.rangeClosed(1, amountOfAvailableRooms.premium())
				.mapToObj(index -> Room.builder().roomStandard(RoomStandard.PREMIUM).index(index).build());
		return Stream.concat(economyRooms, premiumRooms).toList();
	}

	private Income mapRoomsToIncome(List<Room> rooms) {
		List<Room> listOfOccupiedEconomyRooms = rooms.stream()
				.filter(room -> room.isEconomy() && room.isOccupied())
				.toList();
		int numberOfOccupiedEconomyRooms = listOfOccupiedEconomyRooms.size();
		BigDecimal incomeFromEconomyRooms = listOfOccupiedEconomyRooms.stream()
				.map(Room::getOccupiedBy)
				.map(Client::amountToPay)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		RoomOccupancyAndIncome economyRoomOccupancyAndIncome = new RoomOccupancyAndIncome(numberOfOccupiedEconomyRooms, incomeFromEconomyRooms);
		List<Room> listOfOccupiedPremiumRooms = rooms.stream()
				.filter(room -> room.isPremium() && room.isOccupied())
				.toList();
		int numberOfOccupiedPremiumRooms = listOfOccupiedPremiumRooms.size();
		BigDecimal incomeFromPremiumRooms = listOfOccupiedPremiumRooms.stream()
				.map(Room::getOccupiedBy)
				.map(Client::amountToPay)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		RoomOccupancyAndIncome premiumRoomOccupancyAndIncome = new RoomOccupancyAndIncome(numberOfOccupiedPremiumRooms, incomeFromPremiumRooms);
		return new Income(premiumRoomOccupancyAndIncome, economyRoomOccupancyAndIncome);
	}
}
