package pl.beusable.roomoccupancymanager.service.model;

import java.math.BigDecimal;

public record Client(int index, BigDecimal amountToPay) {

	public boolean isStandard() {
		return !isVip();
	}

	public boolean isVip() {
		return new BigDecimal(100).compareTo(amountToPay) < 1;
	}
}
