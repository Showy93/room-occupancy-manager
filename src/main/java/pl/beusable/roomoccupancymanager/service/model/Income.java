package pl.beusable.roomoccupancymanager.service.model;

public record Income(RoomOccupancyAndIncome premium, RoomOccupancyAndIncome economy) {
}
