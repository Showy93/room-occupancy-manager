package pl.beusable.roomoccupancymanager.service.model;

import lombok.Builder;
import lombok.Getter;
import pl.beusable.roomoccupancymanager.service.model.enums.RoomStandard;


@Builder(toBuilder = true)
public class Room {

	private int index;

	@Getter
	private Client occupiedBy;

	private RoomStandard roomStandard;

	public void allocate(Client client) {
		if(occupiedBy != null) {
			throw new UnsupportedOperationException("Room is already occupied by other client");
		}
		occupiedBy = client;
	}

	public boolean isAvailable() {
		return occupiedBy == null;
	}

	public void makeAvailable() {
		occupiedBy = null;
	}

	public boolean isEconomy() {
		return RoomStandard.ECONOMY == roomStandard;
	}

	public boolean isOccupied() {
		return occupiedBy != null;
	}

	public boolean isPremium() {
		return RoomStandard.PREMIUM == roomStandard;
	}
}
