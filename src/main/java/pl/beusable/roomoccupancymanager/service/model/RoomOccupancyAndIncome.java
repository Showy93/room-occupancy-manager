package pl.beusable.roomoccupancymanager.service.model;

import java.math.BigDecimal;

public record RoomOccupancyAndIncome(int occupancy, BigDecimal income) {
}
