package pl.beusable.roomoccupancymanager.service.model.enums;

public enum RoomStandard {
	ECONOMY, PREMIUM;
}
