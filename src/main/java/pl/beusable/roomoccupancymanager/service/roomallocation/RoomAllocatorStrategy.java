package pl.beusable.roomoccupancymanager.service.roomallocation;

import pl.beusable.roomoccupancymanager.service.model.Client;
import pl.beusable.roomoccupancymanager.service.model.Room;

import java.util.List;
import java.util.function.Predicate;

public interface RoomAllocatorStrategy extends Predicate<Client> {

	void allocateClientToAnyRoomIfPossible(Client client, List<Room> rooms);
}
