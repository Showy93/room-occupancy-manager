package pl.beusable.roomoccupancymanager.service.roomallocation;

import org.springframework.stereotype.Component;
import pl.beusable.roomoccupancymanager.service.model.Client;
import pl.beusable.roomoccupancymanager.service.model.Room;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class StandardClientRoomAllocatorStrategy implements RoomAllocatorStrategy {

	@Override
	public void allocateClientToAnyRoomIfPossible(Client client, List<Room> rooms) {
		boolean allocatedToEconomyRoom = allocateToAnyEconomyRoomIfAvailable(client, rooms);
		if(!allocatedToEconomyRoom) {
			allocateToAnyRoomIfUpgradeOfRoomForAnyAllocatedClientAvailable(client, rooms);
		}
	}

	@Override
	public boolean test(Client client) {
		return client.isStandard();
	}

	private boolean allocateToAnyEconomyRoomIfAvailable(Client client, List<Room> rooms) {
		Optional<Room> potentialEconomyRoomToAllocate = rooms.stream()
				.filter(room -> room.isAvailable() && room.isEconomy())
				.findAny();
		potentialEconomyRoomToAllocate.ifPresent(room -> room.allocate(client));
		return potentialEconomyRoomToAllocate.isPresent();
	}

	private boolean allocateToAnyRoomIfUpgradeOfRoomForAnyAllocatedClientAvailable(Client clientNotAllocatedYet, List<Room> rooms) {
		boolean isAnyPremiumRoomAvailable = rooms.stream().anyMatch(room -> room.isPremium() && room.isAvailable());
		if(isAnyPremiumRoomAvailable) {
			allocateToPremiumRoomStandardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom(clientNotAllocatedYet, rooms);
			allocateToAnyEconomyRoomIfAvailable(clientNotAllocatedYet, rooms);
		}
		return isAnyPremiumRoomAvailable;
	}

	private void allocateToPremiumRoomStandardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom(Client clientNotAllocatedYet, List<Room> rooms) {
		Client standardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom =
				findStandardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom(clientNotAllocatedYet, rooms);
		allocateClientToPremiumRoom(standardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom, rooms);
	}

	private void allocateClientToPremiumRoom(Client client, List<Room> rooms) {
		deallocateClientFromOccupiedRoomIfApplicable(client, rooms);
		rooms.stream()
				.filter(room -> room.isPremium() && room.isAvailable())
				.findAny()
				.ifPresent(room -> room.allocate(client));
	}

	private void deallocateClientFromOccupiedRoomIfApplicable(Client client,  List<Room> rooms) {
		rooms.stream()
			.filter(room -> Objects.equals(client, room.getOccupiedBy()))
			.findAny()
			.ifPresent(Room::makeAvailable);
	}

	private Client findStandardClientPayingTheHighestAmountNotAllocatedYetToPremiumRoom(Client notAllocatedClient, List<Room> rooms) {
		Stream<Client> standardClientsAllocatedToEconomyRoom = rooms.stream()
				.filter(room -> room.isEconomy() && room.isOccupied())
				.map(Room::getOccupiedBy);
		return Stream.concat(Stream.of(notAllocatedClient), standardClientsAllocatedToEconomyRoom)
				.max(Comparator.comparing(Client::amountToPay))
				.orElseThrow();
	}
}
