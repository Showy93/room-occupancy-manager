package pl.beusable.roomoccupancymanager.service.roomallocation;

import org.springframework.stereotype.Component;
import pl.beusable.roomoccupancymanager.service.model.Client;
import pl.beusable.roomoccupancymanager.service.model.Room;

import java.util.List;

@Component
public class VipClientRoomAllocatorStrategy implements RoomAllocatorStrategy {
	@Override
	public boolean test(Client client) {
		return client.isVip();
	}

	@Override
	public void allocateClientToAnyRoomIfPossible(Client client, List<Room> rooms) {
		rooms.stream()
				.filter(room -> room.isAvailable() && room.isPremium())
				.findAny()
				.ifPresent(room -> room.allocate(client));
	}
}
