package pl.beusable.roomoccupancymanager.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.beusable.roomoccupancymanager.controller.model.AmountOfAvailableRooms;
import pl.beusable.roomoccupancymanager.service.model.Client;
import pl.beusable.roomoccupancymanager.service.model.Income;
import pl.beusable.roomoccupancymanager.service.model.RoomOccupancyAndIncome;
import pl.beusable.roomoccupancymanager.service.roomallocation.RoomAllocatorStrategy;
import pl.beusable.roomoccupancymanager.service.roomallocation.StandardClientRoomAllocatorStrategy;
import pl.beusable.roomoccupancymanager.service.roomallocation.VipClientRoomAllocatorStrategy;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@Slf4j
@ExtendWith(MockitoExtension.class)
class SimulationServiceTest {

	private static final List<String> STRINGIFIED_AMOUNTS_TO_PAY = List.of("23", "45", "155", "374", "22", "99.99", "100", "101", "115", "209");
	private static final List<Client> MOCKED_CLIENTS = IntStream.range(0,STRINGIFIED_AMOUNTS_TO_PAY.size())
			.mapToObj(index -> new Client(index, new BigDecimal(STRINGIFIED_AMOUNTS_TO_PAY.get(index))))
			.toList();

	private SimulationService simulationService;

	@BeforeEach
	void setup() {
		ClientService clientService = Mockito.mock(ClientService.class);
		when(clientService.getPotentialClients()).thenReturn(MOCKED_CLIENTS);
		List<RoomAllocatorStrategy> roomAllocators = List.of(new StandardClientRoomAllocatorStrategy(), new VipClientRoomAllocatorStrategy());
		simulationService = new SimulationService(clientService, roomAllocators);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/data.csv", numLinesToSkip = 1)
	void calculateExpectedIncome(int freePremiumRooms, int freeEconomyRooms, int usagePremium, BigDecimal incomePremium, int usageEconomy, BigDecimal incomeEconomy) {
		log.info("(input) Free Premium rooms: {}", freePremiumRooms);
		log.info("(input) Free Economy rooms: {}", freeEconomyRooms);
		AmountOfAvailableRooms amountOfAvailableRooms = new AmountOfAvailableRooms(freePremiumRooms, freeEconomyRooms);
		Income expectedIncome = new Income(new RoomOccupancyAndIncome(usagePremium, incomePremium), new RoomOccupancyAndIncome(usageEconomy, incomeEconomy));

		Income income = simulationService.calculateExpectedIncome(amountOfAvailableRooms);

		assertEquals("Expected income different than estimated income", expectedIncome, income);
		log.info("(output) Usage Premium: {} EUR {}", usagePremium, incomePremium);
		log.info("(output) Usage Economy: {} EUR {}", usageEconomy, incomeEconomy);
	}
}